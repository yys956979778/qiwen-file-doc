module.exports = {
  title: '斯米克网盘',
  description: '斯米克内部文件管理系统',
  head: [
    ['link', { rel: 'icon', href: '/QWshare.ico' }]
  ],
  themeConfig: {
    logo: '/img/logo.png',
    nav: [
      { text: '首页', link: '/' },
	  { text: '系统操作指南',
	    items: [
	      { text: '网盘系统', link: '/guide/' },
	      { text: '后台管理', link: '/config/' }
	    ]
	  },
      { text: '常见问题(FAQ)', link: '/question/' },
      { text: '联系 & 支持', link: '/support/' },
      { text: '更新日志', 
        items: [
          { text: '网盘系统', link: '/log/frontend/' },
          { text: '后台管理', link: '/log/backend/' }
        ]
      }
    ],
    sidebarDepth: 2,
    sidebar: {
      '/guide/': [
        '',      /* /guide/ */
        'prepare', /* /guide/prepare.html */
        'install', /* /guide/install.html */
        'development', /* /guide/development.html */
        'deploying', /* /guide/deploying.html */
        'function', /* /guide/function.html */
        'frontend', /* /guide/frontend.html */
      ],
      '/config/': [
        '',      /* /config/ */
      ],
      '/question/': [
        '',      /* /question/ */
      ],
      '/support/': [
        '',      /* /support/ */
      ],
      '/log/': [
        'frontend',      /* /log/frontend.html */
        'backend',      /* /log/backend.html */
      ]
    },
    displayAllHeaders: true,
    lastUpdated: '最近更新时间' // 页面更新时间
  },
  plugins: [
    "@vuepress/plugin-medium-zoom", //  大图查看插件
    'fulltext-search',  //  全局搜索插件
    '@vuepress/back-to-top' //  回到顶部插件
  ]
}