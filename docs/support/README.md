# 联系我们

如果您在使用斯米克网盘的过程中，有任何的疑问可以联系MIS部门。  

# 提建议



 <img class="tip-img" :src="$withBase('/img/support/tip.png')" alt="提示">   
 
 在集团OA中反馈意见及建议给MIS部，谢谢支持。
1. 模块访问路径：
       主菜单”更多“→系统意见箱→工作流EP系统意见箱、SAP系统意见箱、生产制造系统意见箱
	   <br />
	 <img :src="$withBase('/img/x1.png')" alt="微信赞赏">
      
2. 填写说明
       ①左边菜单栏中选择您需要填写的系统
       ②单击“新增”按钮；
       ③填写您的问题、意见建议、上传附件
       ④保存️<br />
 <img :src="$withBase('/img/x2.png')" alt="支付宝赞赏">
      


# 鸣谢

斯米克网盘系统的开发经历了需求分析、项目选型、项目分析、需求整理、项目开发、系统测试、系统操作指南的编写、系统试运行等环节。
	网盘系统经历了从0到1的过程，首先感谢MIS部所有同事，网盘系统的顺利运行离不开大家的意见及建议！


<footer class="page-edit"><!----> <div class="last-updated"><span class="prefix">最近更新时间:</span> <span class="time">2023-04-11, 8:49:15 PM</span></div></footer>